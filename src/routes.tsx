import { createBrowserRouter } from "react-router-dom";

import { App, PageNotFound } from "@pages";
import { MainLayout } from "@layouts";

const routers = createBrowserRouter([
  {
    path: "/",
    element: <MainLayout />,
    children: [
      {
        index: true,
        element: <App />,
      },
    ],
  },
  {
    path: "*",
    element: <PageNotFound />,
  },
]);
export { routers };
