import { Outlet } from "react-router-dom";

const MainLayout = () => {
  return (
    <div className="container font-Sora">
      <Outlet />
    </div>
  );
};

export default MainLayout;
